Rest API for getting exchange rates. It provides mechanism with two types of request:

* by date: /exchangerates/providers/{provider}/pairs/{currency_pair}/date/{date},
* by dates interval: /exchangerates/providers/{provider}/pairs/{currency_pair}/datefrom/{date_from}/dateto/{date_to}.


### Request parameters: ###

 * {provider} - provider alias (see below full list of available providers);
 * {currency_pair} - currency pair for which need provide data (see below full list of available currency pairs);
 * {date} - date for which need provide data;
 * {date_from} - date of beginning interval;
 * {date_to} - date of ending interval.

All dates have format "yyyy-mm-dd". For example: "2016-10-30"


### Available providers: ###

 * fixer (getting data from site *api.fixer.io* by JSON format with using multithread);
 * grandtrunk (getting data from site *currencies.apps.grandtrunk.net* by plain text with using multithread);
 * database (getting data from local database).

Notice: database contains info only for period from 2015-08-21 to 2016-10-14.


### Available currency pairs: ###

 * EURUSD;
 * USDNZD;
 * USDJPY;
 * GBPUSD.