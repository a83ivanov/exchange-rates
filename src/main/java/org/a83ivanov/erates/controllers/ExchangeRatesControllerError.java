/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.erates.controllers;

import org.springframework.http.HttpStatus;

/**
 * Class for customize rest controller response
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
class ExchangeRatesControllerError {
 
    /**
     * Http status
     */
    private final HttpStatus status;
    
    /**
     * Error alias
     */
    private final String error;
 
    /**
     * Base constructor
     * 
     * @param status Http status
     * @param error  Error alias
     */
    public ExchangeRatesControllerError(HttpStatus status, String error) {
        
        this.status = status;
        this.error = error;
    }
    
    /**
     * Constructor for http status only
     * 
     * @param status Http status
     */
    public ExchangeRatesControllerError(HttpStatus status) {
        
        this(status, "");
    }

    /**
     * Getter for http status
     * 
     * @return Http status
     */
    public HttpStatus getStatus() {
        
        return status;
    }
    
    /**
     * Getter for error alias
     * 
     * @return Error alias
     */
    public String getError() {
        
        return error;
    }
}