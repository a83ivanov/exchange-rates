/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.erates.controllers;

import java.util.ArrayList;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.a83ivanov.exchangerates.*;
import org.a83ivanov.exchangerates.exceptions.LoadExchangeRatesException;
import org.a83ivanov.exchangerates.exceptions.WrongCurrencyPairException;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * Rest controller for getting exchnage rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
@RestController
@RequestMapping("/exchangerates")
class ExchangeRatesController {

    /**
     * Provider alias prefix
     * Use for simple mapping short provider name to bean identificator
     */
    private static final String PROVIDER_ALIAS_PREFIX = "provider_";
    
    /**
     * Context of application
     */
    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Message source for localization
     */
    @Autowired
    private MessageSource messageSource;
    
    /**
     * Class for logger lazy load
     */
    private static class ErrorLoggerInstance {
        
        /**
         * Logger object of CurrencyRatesController class
         */
        private static final Logger LOGGER =
            Logger.getLogger(ExchangeRatesController.class); 
        
        /**
         * Getter for logger
         * 
         * @return Logger
         */
        public static Logger getLogger() {
            
            return LOGGER;
        }
    }    
    
    /**
     * Class of items for mapping exceptions in error messages
     */
    private static class ErrorsMapItem {
        
        /**
         * Exception class
         */
        Class baseClass;
        
        /**
         * Http status that would be returned
         */
        HttpStatus httpStatus;
        
        /**
         * Alias of message that would be showed
         */
        String errorKey;
        
        /**
         * Constructor
         * 
         * @param baseClass     Exception class
         * @param httpStatus    Http status
         * @param errorKey      Message alias
         */
        ErrorsMapItem(Class baseClass, HttpStatus httpStatus, String errorKey) {
            
            this.baseClass = baseClass;
            this.httpStatus = httpStatus;
            this.errorKey = errorKey;
        }
    }
    
    /**
     * Prepared list for mapping exceptions in error messages
     */
    static private final List<ErrorsMapItem> ERRORS_MAP = new ArrayList<>();
    static {
        ERRORS_MAP.add(new ErrorsMapItem(
            MethodArgumentTypeMismatchException.class, HttpStatus.BAD_REQUEST,
            "app.errors.bad_request"
        ));        
        ERRORS_MAP.add(new ErrorsMapItem(
            BeansException.class, HttpStatus.NOT_FOUND,
            "app.errors.wrong_provider"
        ));
        ERRORS_MAP.add(new ErrorsMapItem(
            WrongCurrencyPairException.class, HttpStatus.NOT_FOUND,
            "app.errors.wrong_currency_pair"
        ));
        ERRORS_MAP.add(new ErrorsMapItem(
            LoadExchangeRatesException.class, HttpStatus.INTERNAL_SERVER_ERROR,
            "app.errors.loading_error"
        ));
        ERRORS_MAP.add(new ErrorsMapItem(
            Exception.class, HttpStatus.INTERNAL_SERVER_ERROR,
            "app.errors.internal_error"
        ));
    }

    /**
     * Method that handles request for getting exchange rates by date
     * 
     * @param provider  Provider shortname
     * @param pair      Currency pair
     * @param date      Date
     * @return List of exchange rates
     * @throws LoadExchangeRatesException
     * @throws WrongCurrencyPairException 
     */
    @RequestMapping(
        value = "/providers/{provider:[\\w]+}/" +
                "pairs/{pair:[\\w]{6}}/" +
                "date/{date:[\\d]{4}\\-[\\d]{2}\\-[\\d]{2}}",
        method = RequestMethod.GET
    )
    public List<ExchangeRate> getRatesByDate(
        @PathVariable("provider") String provider,
        @PathVariable("pair") String pair,
        @PathVariable("date") @DateTimeFormat(iso=ISO.DATE) Date date
    ) throws LoadExchangeRatesException,
             WrongCurrencyPairException {
        
        return getRatesByDatesInterval(provider, pair, date, date);
    }

    /**
     * Method that handles request for getting exchange rates by dates interval
     * 
     * @param provider  Provider shortname
     * @param pair      Currency pair
     * @param dateFrom  Start of dates interval
     * @param dateTo    End of dates interval
     * @return List of exchange rates
     * @throws LoadExchangeRatesException
     * @throws WrongCurrencyPairException 
     */
    @RequestMapping(
        value = "/providers/{provider:[\\w]+}/" + 
                "pairs/{pair:[\\w]{6}}/" +
                "datefrom/{datefrom:[\\d]{4}\\-[\\d]{2}\\-[\\d]{2}}/" +
                "dateto/{dateto:[\\d]{4}\\-[\\d]{2}\\-[\\d]{2}}",
        method = RequestMethod.GET
    )
    public List<ExchangeRate> getRatesByDatesInterval(
        @PathVariable("provider") String provider,
        @PathVariable("pair") String pair,
        @PathVariable("datefrom") @DateTimeFormat(iso=ISO.DATE) Date dateFrom,
        @PathVariable("dateto") @DateTimeFormat(iso=ISO.DATE) Date dateTo
    ) throws LoadExchangeRatesException,
             WrongCurrencyPairException {
        
        ExchangeRatesProvider exchangeRatesProvider = applicationContext.getBean(
            PROVIDER_ALIAS_PREFIX.concat(provider.toLowerCase()),
            ExchangeRatesProvider.class
        );

        return exchangeRatesProvider.get(
            new CurrencyPair(pair.toUpperCase()), dateFrom, dateTo
        );        
    }
    
    /**
     * Exceptions handler
     * 
     * @param exception Exception object
     * @param response  Response object
     * @return Customized response object
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ExchangeRatesControllerError handleException(Exception exception,
            HttpServletResponse response) {
        
        ErrorLoggerInstance.getLogger().error(exception);
        
        for (ErrorsMapItem mapItem : ERRORS_MAP) {
            if(mapItem.baseClass.isAssignableFrom(exception.getClass())) {
                response.setStatus(mapItem.httpStatus.value());
                return new ExchangeRatesControllerError(
                    mapItem.httpStatus,
                    messageSource.getMessage(
                        mapItem.errorKey, null, LocaleContextHolder.getLocale()
                    )
                );                
            }
        }
        
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        return new ExchangeRatesControllerError(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
