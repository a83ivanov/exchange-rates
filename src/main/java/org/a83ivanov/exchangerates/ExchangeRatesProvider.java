/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates;

import java.util.Date;
import java.util.List;
import org.a83ivanov.exchangerates.exceptions.LoadExchangeRatesException;

/**
 * Interface for exchange rates providers
 * 
 * @author Ivanov Andrey
 * @version 1.0
 */
public interface ExchangeRatesProvider {
    
    /**
     * Method gets list of exchange rates 
     * 
     * @param pair      Currency pair
     * @param dateFrom  Start of interval for getting data
     * @param dateTo    End of interval for getting data
     * @return List of exchange rates
     * @throws LoadExchangeRatesException 
     */
    List<ExchangeRate> get(CurrencyPair pair, Date dateFrom, Date dateTo)
            throws LoadExchangeRatesException;

}
