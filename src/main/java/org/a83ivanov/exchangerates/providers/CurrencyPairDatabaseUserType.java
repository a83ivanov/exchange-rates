/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.providers;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.a83ivanov.exchangerates.CurrencyPair;
import org.a83ivanov.exchangerates.exceptions.WrongCurrencyPairException;

/**
 * Customized type for currency pair persistence
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class CurrencyPairDatabaseUserType implements UserType {

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<CurrencyPair> returnedClass() {
        
        return CurrencyPair.class;
    }
 
    /**
     * {@inheritDoc}
     */
    @Override
    public int[] sqlTypes() {
        
        return new int[] { Types.CHAR };
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMutable() {
        
        return false;
    }
 
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        
        return x.equals(y);
    }
     
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode(Object x) throws HibernateException {
        
        assert (x != null);
        return x.hashCode();
    }
 
    /**
     * {@inheritDoc}
     */
    @Override
    public Object deepCopy(Object value) throws HibernateException {
        
        return value;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Object replace(Object original, Object target, Object owner)
            throws HibernateException {
        
        return original;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        
        return (Serializable) value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object assemble(Serializable cached, Object owner)
            throws HibernateException {
        
        return cached;
    }    

    /**
     * {@inheritDoc}
     */
    @Override
    public Object nullSafeGet(ResultSet rs, String[] strings,
            SharedSessionContractImplementor ssci, Object o)
            throws HibernateException,
                   SQLException {
        
        String pair = rs.getString(strings[0]);
        
        if(pair != null) {
            try {
                return new CurrencyPair(pair);
            } catch(WrongCurrencyPairException e) {}
        }
        
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void nullSafeSet(PreparedStatement ps, Object o, int i,
            SharedSessionContractImplementor ssci)
            throws HibernateException,
                   SQLException {
        
        if(o == null) {
            ps.setNull(i, Types.CHAR);
        } else {
            ps.setString(i, o.toString());
        }
    }
    
}
