/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.providers;

import java.util.Date;
import java.util.List;
import org.a83ivanov.exchangerates.CurrencyPair;
import org.a83ivanov.exchangerates.exceptions.LoadExchangeRatesException;
import org.a83ivanov.exchangerates.ExchangeRate;
import org.a83ivanov.exchangerates.ExchangeRatesProvider;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Provider for getting exchange rates from database
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
class DatabaseProvider implements ExchangeRatesProvider {

    /**
     * Service that implement work with database
     */
    private final ExchangeRateDatabaseEntityService exchangeRateEntityService;
    
    /**
     * Constructor
     * 
     * @param exchangeRateEntityService Service for work with database
     */
    @Autowired
    public DatabaseProvider(ExchangeRateDatabaseEntityService exchangeRateEntityService) {
        
        this.exchangeRateEntityService = exchangeRateEntityService;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExchangeRate> get(CurrencyPair pair, Date dateFrom, Date dateTo)
            throws LoadExchangeRatesException {
      
        return (List<ExchangeRate>) exchangeRateEntityService
            .getByDatesIntervalAndPair(
                new java.sql.Date(dateFrom.getTime()),
                new java.sql.Date(dateTo.getTime()), pair.toString()
            );
    }

}