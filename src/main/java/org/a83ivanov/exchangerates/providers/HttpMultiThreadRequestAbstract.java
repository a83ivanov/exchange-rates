/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.providers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * Abstract class of objects for multithread http requests
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
abstract class HttpMultiThreadRequestAbstract {

    /**
     * Return the close price
     * 
     * @param firstCurrencyCode     Code of the first currency 
     * @param secondCurrencyCode    Code of the second currency 
     * @param date                  Date for getting exchange rate
     * @return Close price
     * @throws IOException 
     * @throws MalformedURLException
     */
    public final float getClosePrice(String firstCurrencyCode,
            String secondCurrencyCode, Date date)
            throws IOException,
                   MalformedURLException {
        
        URL url = getUrl(firstCurrencyCode, secondCurrencyCode, date);
        return request(url, firstCurrencyCode, secondCurrencyCode);
    }
    
    /**
     * Create Url for getting exchange rate
     * 
     * @param firstCurrencyCode     Code of the first currency 
     * @param secondCurrencyCode    Code of the second currency 
     * @param date                  Date for getting exchange rate
     * @return Url
     * @throws MalformedURLException 
     */
    abstract protected URL getUrl(String firstCurrencyCode,
            String secondCurrencyCode, Date date) throws MalformedURLException;
    
    /**
     * Request to a web service by http for getting exchange rates
     *       
     * @param url                   Request Url
     * @param firstCurrencyCode     Code of the first currency 
     * @param secondCurrencyCode    Code of the second currency 
     * @return Close price
     * @throws IOException
     */
    abstract protected float request(URL url, String firstCurrencyCode,
            String secondCurrencyCode) throws IOException;
    
}
