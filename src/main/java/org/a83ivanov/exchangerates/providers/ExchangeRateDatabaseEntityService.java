/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.providers;

import java.sql.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Predicate;
import org.hibernate.SessionFactory;
import javax.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.a83ivanov.exchangerates.ExchangeRate;

/**
 * Database entity service
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
@Repository
@Transactional
class ExchangeRateDatabaseEntityService {

    /**
     * Hibernate session factory
     */
    private SessionFactory sessionFactory;
    
    /**
     * Hibernate entity manager factory
     */
    private EntityManagerFactory entityManagerFactory;
    
    /**
     * Setter for session factory
     * 
     * @param sessionFactory Session factory object
     */
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * Setter for entity manager factory
     * 
     * @param entityManagerFactory Entity manager factory object
     */
    @Autowired
    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        
        this.entityManagerFactory = entityManagerFactory;
    }
    
    /**
     * Method returns list of exchange rates from database
     * 
     * @param dateFrom  Start date of interval for getting data
     * @param dateTo    End date of interval for getting data
     * @param pair      Currency pair for getting data
     * @return List of exchange rates
     */
    @Transactional(readOnly=true)
    public List<? extends ExchangeRate> getByDatesIntervalAndPair(
            Date dateFrom, Date dateTo, String pair) {
        
        this.sessionFactory.openSession();

        CriteriaBuilder criteriaBuilder = entityManagerFactory.getCriteriaBuilder();
        CriteriaQuery<ExchangeRateDatabaseEntity> criteriaQuery =
            criteriaBuilder.createQuery(ExchangeRateDatabaseEntity.class);
        Root<ExchangeRateDatabaseEntity> root = 
            criteriaQuery.from(ExchangeRateDatabaseEntity.class);
        criteriaQuery.select(root);
        Predicate criteria = criteriaBuilder.conjunction();

        criteria = criteriaBuilder.and(
            criteria,    
            criteriaBuilder.between(
                root.get(ExchangeRateDatabaseEntity_.date), dateFrom, dateTo
            )
        );

        criteria = criteriaBuilder.and(
            criteria,    
            criteriaBuilder.equal(
                root.get(ExchangeRateDatabaseEntity_.pair), pair
            )
        );

        return entityManagerFactory
            .createEntityManager()
            .createQuery(criteriaQuery.where(criteria))
            .getResultList();
    }
    
}
