/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.providers;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import org.a83ivanov.exchangerates.ExchangeRate;
import org.a83ivanov.exchangerates.CurrencyPair;
import org.hibernate.annotations.Type;

/**
 * Entity class of exchange rates from database
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
@Entity
@Table(name="rates")
@IdClass(ExchangeRateDatabaseEntityId.class)
class ExchangeRateDatabaseEntity extends ExchangeRate implements Serializable {

    /**
     * {@inheritDoc}
     */
    @Id
    @Column(name="date")
    @Override
    public Date getDate() {
        
        return super.getDate();
    }
    
    /**
     * {@inheritDoc}
     */
    @Id
    @Type(type = "org.a83ivanov.exchangerates.providers.CurrencyPairDatabaseUserType")
    @Column(name="pair", columnDefinition="CHAR(8)")
    @Override
    public CurrencyPair getPair() {
        
        return super.getPair();
    }

    /**
     * {@inheritDoc}
     */
    @Column(name="high_price", precision=12, scale=6)
    @Override
    public float getHigh() {
        
        return super.getHigh();
    }

    /**
     * {@inheritDoc}
     */
    @Column(name="low_price", precision=12, scale=6)
    @Override
    public float getLow() {
        
        return super.getLow();
    }

    /**
     * {@inheritDoc}
     */
    @Column(name="open_price", precision=12, scale=6)
    @Override
    public float getOpen() {
        
        return super.getOpen();
    }

    /**
     * {@inheritDoc}
     */
    @Column(name="close_price", precision=12, scale=6)
    @Override
    public float getClose() {
        
        return super.getClose();
    }

}