/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.providers;

import org.a83ivanov.exchangerates.ExchangeRate;
import org.a83ivanov.exchangerates.ExchangeRatesProvider;
import org.a83ivanov.exchangerates.exceptions.LoadExchangeRatesException;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import org.a83ivanov.exchangerates.CurrencyPair;
import java.util.concurrent.TimeUnit;
import java.util.Calendar;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.net.MalformedURLException;
import java.util.Comparator;

/**
 * Handler for getting exchange rates by HTTP with multithreading
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
final public class HttpMultiThreadProvider implements ExchangeRatesProvider {
    
    /**
     * Request object for getting data from web service
     */
    private final HttpMultiThreadRequestAbstract request;
    
    /**
     * Constructor of handler
     * 
     * <p>See {@link org.a83ivanov.exchangerates.handlers.HttpMultiThreadRequestAbstract#HttpMultiThreadRequestAbstract()}</p>
     * 
     * @param requestObject Request object
     */
    public HttpMultiThreadProvider(HttpMultiThreadRequestAbstract requestObject) {
        
        request = requestObject;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExchangeRate> get(CurrencyPair pair, Date dateFrom, Date dateTo)
            throws LoadExchangeRatesException {

        Set<ExchangeRate> exchnageRates = 
            new TreeSet<>(new Comparator<ExchangeRate>(){
                @Override
                public int compare(ExchangeRate rate1, ExchangeRate rate2) {

                    return rate1.getDate().compareTo(rate2.getDate());
                }
            });
        
        if(dateTo.before(dateFrom)) {
            throw new LoadExchangeRatesException();
        }
        
        long days = TimeUnit.DAYS.convert(
            dateTo.getTime() - dateFrom.getTime(),
            TimeUnit.MILLISECONDS
        ) + 1;
        
        ExecutorService pool = Executors.newFixedThreadPool((int) days);
        Set<Future<ExchangeRate>> set = new HashSet<>();

        for(int i=0; i < days; i++) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFrom);
            cal.add(Calendar.DATE, i);
            Date requestDate = cal.getTime();
            set.add(
                pool.submit(
                    new HttpMultiThreadHandlerCallable(request, pair, requestDate)
                )
            );
        }

        for(Future<ExchangeRate> future : set){
            try {
                exchnageRates.add(future.get());
            } catch(InterruptedException | ExecutionException exception){
                throw new LoadExchangeRatesException(exception);
            }
        }
        
        return new ArrayList<>(exchnageRates);
    }

    /**
     * Class of callable objects for doing requests
     * 
     * @author  Ivanov Andrey
     * @version 1.0
     */
    private final static class HttpMultiThreadHandlerCallable
            implements Callable {
        
        /**
         * Request object for getting data from web service
         */
        private final HttpMultiThreadRequestAbstract request;
        
        /**
         * Currency pair
         */
        private final CurrencyPair pair;
        
        /**
         * Date for getting rates
         */
        private final Date date;
        
        /**
         * Constructor
         * 
         * @param requestObject Request object
         * @param currencyPair  Currency pair
         * @param rateDate      Date for getting rates
         */
        public HttpMultiThreadHandlerCallable(
                HttpMultiThreadRequestAbstract requestObject,
                CurrencyPair currencyPair, Date rateDate) {

            request = requestObject;
            pair    = currencyPair;
            date    = rateDate;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public ExchangeRate call() throws IOException, MalformedURLException {
            
            float closePrice = request.getClosePrice(
                pair.getFirstCode(), pair.getSecondCode(), date
            );

            return new ExchangeRate(pair, new java.sql.Date(date.getTime()), closePrice);
        }
    }
}
