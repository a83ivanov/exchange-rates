/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.providers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.net.URL;
import java.net.HttpURLConnection;

/**
 * Class of objects for doing requests to Grandtrunk
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
class GrandtrunkProviderRequest extends HttpMultiThreadRequestAbstract {
    
    /**
     * Template for request url
     */
    private final String URL_TEMPLATE = 
        "http://currencies.apps.grandtrunk.net/getrate/%s/%s/%s";
    
    /**
     * Timeout of reading page
     */
    private static final int READING_PAGE_TIMEOUT = 10000;
    
    /**
     * HTTP request method
     */
    private static final String REQUEST_METHOD = "GET";
    
    /**
     * Date format for request
     */
    private static final String REQUEST_DATE_FORMAT = "yyyy-MM-dd";
    
    /**
     * {@inheritDoc}
     */    
    @Override
    protected URL getUrl(String firstCurrencyCode,
            String secondCurrencyCode, Date date)
            throws MalformedURLException {
        
        return new URL(
            String.format(
                URL_TEMPLATE,
                new SimpleDateFormat(REQUEST_DATE_FORMAT).format(date),
                firstCurrencyCode,
                secondCurrencyCode
            )
        );
    }
    
    /**
     * {@inheritDoc}
     */    
    @Override
    protected float request(URL url, String firstCurrencyCode,
            String secondCurrencyCode) throws IOException {
        
        String line;
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(REQUEST_METHOD);
        connection.setReadTimeout(READING_PAGE_TIMEOUT);
        connection.connect();

        try (    
            InputStreamReader inputStreamReader =
                    new InputStreamReader(connection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            line = bufferedReader.readLine();
        }    

        connection.disconnect();
        
        return Float.parseFloat(line);
    }

}
