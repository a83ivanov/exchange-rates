/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.providers;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;
import org.a83ivanov.exchangerates.CurrencyPair;

/**
 * Composite primary key for exchange rate entity
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class ExchangeRateDatabaseEntityId implements Serializable {

    /**
     * Date of exchange rate
     */
    private Date date;
    
    /**
     * Currency pair
     */
    private CurrencyPair pair;

    /**
     * Default constructor
     */
    public ExchangeRateDatabaseEntityId() {}

    /**
     * Extended constructor
     * 
     * @param date  Date of exchange rate
     * @param pair  Currency pair
     */
    public ExchangeRateDatabaseEntityId(Date date, CurrencyPair pair) {

        this.date = date;
        this.pair = pair;
    }

    /**
     * Getter for date of exchange rate
     * 
     * @return Date
     */
    public Date getDate() {

        return date;
    }

    /**
     * Setter for date of exchange rate
     * 
     * @param date Date
     */
    public void setDate(Date date) {

        this.date = date;
    }

    /**
     * Getter for currency pair of exchange rate
     * 
     * @return Currency pair
     */
    public CurrencyPair getPair() {

        return pair;
    }

    /**
     * Setter for currency pair of exchange rate
     * 
     * @param pair 
     */
    public void setPair(CurrencyPair pair) {

        this.pair = pair;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        return Objects.hash(getDate(), getPair());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {

        return Objects.equals(this, obj);
    }    
}