/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.providers;

import java.net.MalformedURLException;
import java.util.Date;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.io.InputStream;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonReader;
import javax.json.JsonObject;

/**
 * Class of objects for doing requests to Fixer
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
class FixerProviderRequest extends HttpMultiThreadRequestAbstract {

    /**
     * Template for request url
     */    
    private static final String URL_TEMPLATE =
        "http://api.fixer.io/%s?base=%s&symbols=%s";
    
    /**
     * Date format for request
     */    
    private static final String REQUEST_DATE_FORMAT = "yyyy-MM-dd";
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected URL getUrl(String firstCurrencyCode,
            String secondCurrencyCode, Date date) throws MalformedURLException {
        
        return new URL(String.format(
            URL_TEMPLATE, new SimpleDateFormat(REQUEST_DATE_FORMAT).format(date),
            firstCurrencyCode, secondCurrencyCode
        ));
    }
    
    /**
     * {@inheritDoc}
     */    
    @Override
    protected float request(URL url, String firstCurrencyCode,
            String secondCurrencyCode) throws IOException {

        float closePrice;
        
        try (
            InputStream is = url.openStream();
            JsonReader rdr = Json.createReader(is)
        ) {
            JsonObject obj = rdr.readObject();
            closePrice = (float) obj
                .getJsonObject("rates")
                .getJsonNumber(secondCurrencyCode)
                .doubleValue();
        }
        
        return closePrice;
    }
}
