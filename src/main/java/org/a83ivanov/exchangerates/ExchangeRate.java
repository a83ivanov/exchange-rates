/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates;

import java.text.SimpleDateFormat;
import java.sql.Date;

/**
 * Class of exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class ExchangeRate {
    
    /**
     * Date of exchange rate
     */
    private Date date;
    
    /**
     * Currency pair
     */
    private CurrencyPair pair;
    
    /**
     * High price
     */
    private float high;
    
    /**
     * Low price
     */
    private float low;
    
    /**
     * Open price
     */
    private float open;
    
    /**
     * Close price
     */
    private float close;
    
    /**
     * Default consrtuctor
     */
    public ExchangeRate() {}
    
    /**
     * Constructor for close price only
     * 
     * @param pair          Currency pair
     * @param rateDate      Date of exchange rate
     * @param closePrice    Close price
     */
    public ExchangeRate(CurrencyPair pair, Date rateDate, float closePrice) {
        
        this.pair  = pair;
        this.date  = rateDate;
        this.close = closePrice;
    }
    
    /**
     * Extended constructor
     * 
     * @param pair          Currency pair
     * @param rateDate      Date of exchange rate
     * @param highPrice     High price
     * @param lowPrice      Low price
     * @param openPrice     Open price
     * @param closePrice    Close price
     */
    public ExchangeRate(CurrencyPair pair, Date rateDate, float highPrice,
            float lowPrice, float openPrice, float closePrice) {
        
        this.pair  = pair;
        this.date  = rateDate;
        this.high  = highPrice;
        this.low   = lowPrice;
        this.open  = openPrice;
        this.close = lowPrice;
    }

    /**
     * Setter for currency pair
     * 
     * @param currencyPair Currency pair
     */
    public void setPair(CurrencyPair currencyPair) {
        
        pair = currencyPair;
    }
    
    /**
     * Setter for exchange rate date
     * 
     * @param rateDate Date of exchange rate
     */
    public void setDate(Date rateDate) {
        
        date = rateDate;
    }
    
    /**
     * Setter for high price
     * 
     * @param highPrice High price
     */
    public void setHigh(float highPrice) {
        
        high = highPrice;
    }
    
    /**
     * Setter for low price
     * 
     * @param lowPrice Low price
     */
    public void setLow(float lowPrice) {
        
        low = lowPrice;
    }
    
    /**
     * Setter for open price
     * 
     * @param openPrice Open price
     */
    public void setOpen(float openPrice) {
        
        open = openPrice;
    }
    
    /**
     * Setter for close price
     * 
     * @param closePrice Close price
     */
    public void setClose(float closePrice) {
        
        close = closePrice;
    }

    /**
     * Return currency pair of exchange rate
     * 
     * @return CurrencyPair currency pair of this exchange rate
     */
    public CurrencyPair getPair() {
        
        return pair;
    }
    
    /**
     * Return date of exchange rate
     * 
     * @return Date date of this exchange rate
     */
    public Date getDate() {
        
        return date;
    }
    
    /**
     * Return formatted date
     * 
     * See <a href="https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html">Date and time patterns</a>
     * 
     * @param format Format
     * @return String formatted date of this exchange rate
     */
    public String getDate(String format) {
        
        return new SimpleDateFormat(format).format(date);
    }
    
    /**
     * Return high price
     * 
     * @return float high price
     */
    public float getHigh() {
        
        return high;
    }
    
    /**
     * Return low price
     * 
     * @return float low price
     */
    public float getLow() {
        
        return low;
    }
    
    /**
     * Return open price
     * 
     * @return float open price
     */
    public float getOpen() {
        
        return open;
    }
    
    /**
     * Return close price
     * 
     * @return float close price
     */
    public float getClose() {
        
        return close;
    }
}
