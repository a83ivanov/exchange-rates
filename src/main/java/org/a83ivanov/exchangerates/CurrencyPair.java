/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.a83ivanov.exchangerates.exceptions.WrongCurrencyPairException;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import java.util.Objects;

/**
 * Class of currency pair objects
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
@JsonSerialize(using = CurrencyPairSerializer.class)
public class CurrencyPair implements Serializable {
    
    /**
     * List of available currency pairs
     */
    private static final List<String> AVAILABLE_CURRENCY_PAIRS = new ArrayList<>();
    static {
        AVAILABLE_CURRENCY_PAIRS.add("EURUSD");
        AVAILABLE_CURRENCY_PAIRS.add("USDNZD");
        AVAILABLE_CURRENCY_PAIRS.add("USDJPY");
        AVAILABLE_CURRENCY_PAIRS.add("GBPUSD");
    }

    /**
     * The first (main) currency ISO-4217 code
     */
    private final String firstCurrency;
    
    /**
     * The second currency ISO-4217 code
     */
    private final String secondCurrency;
    
    /**
     * Constructor for currency pair
     * 
     * <p>Use {@link getAvailableCurrencyPairs()} for getting list of available
     * currency pairs.</p>
     * 
     * @param pair Currency pair
     * @throws WrongCurrencyPairException if currency pair isn`t correct
     */
    public CurrencyPair(String pair) throws WrongCurrencyPairException {
        
        this(pair.substring(0, 3), pair.substring(3));
    }
    
    /**
     * Constructor for ISO codes
     * 
     * <p>See <a href="https://ru.wikipedia.org/wiki/ISO_4217">https://ru.wikipedia.org/wiki/ISO_4217</a></p>
     * <p>Use {@link getAvailableCurrencyPairs()} for getting list of available
     * currency pairs.</p>
     * 
     * @param firstCode     ISO code of the first currency
     * @param secondCode    ISO code of the second currency
     * @throws WrongCurrencyPairException if currency pair isn`t correct
     */
    public CurrencyPair(String firstCode, String secondCode)
            throws WrongCurrencyPairException {
        
        firstCurrency  = firstCode.toUpperCase();
        secondCurrency = secondCode.toUpperCase();

        validateCurrencyPair(firstCurrency + secondCurrency);
    }
    
    /**
     * Return ISO code for the first currency
     * 
     * <p>See <a href="https://ru.wikipedia.org/wiki/ISO_4217">https://ru.wikipedia.org/wiki/ISO_4217</a></p>
     * 
     * @return String this pair the first currency ISO code
     */
    public String getFirstCode() {
        
        return firstCurrency;
    }
    
    /**
     * Return ISO code for the second currency.
     * 
     * <p>See <a href="https://ru.wikipedia.org/wiki/ISO_4217">https://ru.wikipedia.org/wiki/ISO_4217</a></p>
     * 
     * @return String this pair the second currency ISO code
     */
    public String getSecondCode() {
        
        return secondCurrency;
    }
    
    /**
     * Return currency pair as string
     * 
     * <p>For example: if the first currency is <b>EUR</b> and the second currency
     * id <b>USD</b>, this method will return string <b>EURUSD</b>.</p>
     * 
     * @return String
     */
    @Override
    public String toString() {
        
        return new StringBuilder(firstCurrency)
            .append(secondCurrency)
            .toString();
    }
    
    /**
     * Return the list of available currency pairs
     * 
     * @return String[]
     */
    public static String[] getAvailableCurrencyPairs() {
        
        return AVAILABLE_CURRENCY_PAIRS.toArray(
            new String[AVAILABLE_CURRENCY_PAIRS.size()]
        );
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {

        return Objects.equals(this.toString(), obj.toString());
    }  
    
    /**
     * Checking for available currency pair
     * 
     * @param pair Currency pair (in the uppercase)
     * @throws WrongCurrencyPairException if currency pair not available for
     * getting rates
     */
    private void validateCurrencyPair(String pair)
            throws WrongCurrencyPairException {
        
        if(!AVAILABLE_CURRENCY_PAIRS.contains(pair)) {
            throw new WrongCurrencyPairException(pair);
        }
    }
}
