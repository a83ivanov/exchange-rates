/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.exceptions;

/**
 * Base class for exceptions
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
abstract public class BaseException extends Exception {
 
    /**
     * {@inheritDoc}
     */
    public BaseException() {
        
        super();
    }
    
    /**
     * {@inheritDoc}
     */
    public BaseException(String message) {
        
        super(message);
    }
    
    /**
     * {@inheritDoc}
     */    
    public BaseException(Throwable cause) {
        
        super(cause);
    }
}
