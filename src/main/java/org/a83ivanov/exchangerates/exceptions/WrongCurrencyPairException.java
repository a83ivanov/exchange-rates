/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.exceptions;

/**
 * Wrong currency pair exception
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class WrongCurrencyPairException extends BaseException {

    /**
     * {@inheritDoc}
     */
    public WrongCurrencyPairException() {
        
        super();
    }
    
    /**
     * {@inheritDoc}
     */
    public WrongCurrencyPairException(String message) {
        
        super(message);
    }
    
}
