/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.exceptions;

/**
 * Error on loading exchange rates exception
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class LoadExchangeRatesException extends BaseException {
    
    /**
     * {@inheritDoc}
     */
    public LoadExchangeRatesException() {
        
        super();
    }
    
    /**
     * {@inheritDoc}
     */    
    public LoadExchangeRatesException(Throwable cause) {
        
        super(cause);
    }
    
}
