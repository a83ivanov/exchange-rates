/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates.exceptions;

/**
 * Wrong handler exception
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class WrongHandlerException extends BaseException {

    /**
     * {@inheritDoc}
     */
    public WrongHandlerException() {
        
        super();
    }
    
    /**
     * {@inheritDoc}
     */
    public WrongHandlerException(String message) {
        
        super(message);
    }
    
}
