/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

package org.a83ivanov.exchangerates;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;

/**
 * Serializer for currency pair objects
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
class CurrencyPairSerializer extends JsonSerializer<CurrencyPair> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(CurrencyPair value, JsonGenerator generator,
            SerializerProvider provider)
            throws IOException,
                   JsonProcessingException {

        generator.writeString(value.toString());
    }
}
