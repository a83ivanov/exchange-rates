/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.server.MockMvc;
import org.springframework.test.web.server.setup.MockMvcBuilders;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * Tests for controller methods
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class ControllerNGTest {
    
    /**
     * Application context file
     */
    private final static String APP_CONTEXT =
        "file:src/main/webapp/WEB-INF/dispatcher-servlet.xml";
    
    /**
     * Application context object
     */
    private ClassPathXmlApplicationContext ctx;
    
    /**
     * Mock for MVC
     */
    private MockMvc mockMvc;
    
    /**
     * Data provider method that returns list of wrong urls
     * 
     * @return Array of objects data
     */
    @DataProvider
    public Object[][] wrongUrls() {
        
        return new Object[][] {
            {"/providers", HttpMethod.GET, HttpStatus.NOT_FOUND},
            {"/exchangerates/providers", HttpMethod.GET, HttpStatus.NOT_FOUND},
            {"/", HttpMethod.GET, HttpStatus.NOT_FOUND},
            {"/exchangerates/providers/database/pairs/eurusd/date/2016-10-10", HttpMethod.POST, HttpStatus.METHOD_NOT_ALLOWED},
            {"/exchangerates/providers/database/pairs/eurusd/date/2016-10-10", HttpMethod.PUT, HttpStatus.METHOD_NOT_ALLOWED},
            {"/exchangerates/providers/database/pairs/eurusd/date/2016-10-10", HttpMethod.DELETE, HttpStatus.METHOD_NOT_ALLOWED},
            {"/exchangerates/providers/database/pairs/eurusd/date/2016-10-10", HttpMethod.PATCH, HttpStatus.METHOD_NOT_ALLOWED},
        };
    }  
    
    /**
     * Check errors on wrong requests
     * 
     * @param url    Request URL
     * @param method Request method
     * @param status Result http status
     * @throws Exception 
     */
    @Test(
        dataProvider = "wrongUrls"
    )
    public void pageNotFound(String url, HttpMethod method, HttpStatus status) throws Exception {
        
        this.mockMvc
            .perform(request(method, url))
            .andExpect(status().is(status.value()));
    }
    
    /**
     * Data provider mathod that returns list of correct urls
     * 
     * @return Array of objects data
     */
    @DataProvider
    public Object[][] correctUrls() {

        return new Object[][] {
            {
                "/exchangerates/providers/database/pairs/eurusd/date/2016-10-10",
                "getRatesByDate"
            },
            {
                "/exchangerates/providers/database/pairs/gbpusd/date/2016-10-10",
                "getRatesByDate"
            },
            {
                "/exchangerates/providers/fixer/pairs/eurusd/date/2016-10-30",
                "getRatesByDate"
            },
            {
                "/exchangerates/providers/database/pairs/eurusd/datefrom/2016-10-10/dateto/2016-10-11",
                "getRatesByDatesInterval"
            },
            {
                "/exchangerates/providers/database/pairs/gbpusd/datefrom/2016-10-10/dateto/2016-10-12",
                "getRatesByDatesInterval"
            },
            {
                "/exchangerates/providers/fixer/pairs/eurusd/datefrom/2016-10-30/dateto/2016-11-05",
                "getRatesByDatesInterval"
            }          
        };
    }    
    
    /**
     * Check invoke of controller methods
     * 
     * @param url    Request URL
     * @param method Controller method that must be invoked
     * @throws Exception 
     */
    @Test(
        dataProvider="correctUrls"
    )
    public void checkCorrectRequests(String url, String method) throws Exception {

        this.mockMvc
            .perform(get(url))
            .andExpect(handler().methodName(method))
            .andExpect(status().isOk())
            .andReturn();
    }
    
    @BeforeClass
    public void setUpClass() {

        this.mockMvc = MockMvcBuilders
            .xmlConfigSetup(APP_CONTEXT)
            .build();
    }
    
    @AfterClass
    public void tearDownClass() {
        
        this.mockMvc = null;
    }        
}
