/**
 * Rest Api for getting exchange rates
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */

import java.text.ParseException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.a83ivanov.exchangerates.*;
import org.a83ivanov.exchangerates.exceptions.WrongCurrencyPairException;
import org.a83ivanov.exchangerates.exceptions.LoadExchangeRatesException;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Tests for providers
 * 
 * @author  Ivanov Andrey
 * @version 1.0
 */
public class ProvidersNGTest extends Assert {
    
    /**
     * Application context file
     */
    private final static String APP_CONTEXT =
        "src/main/webapp/WEB-INF/dispatcher-servlet.xml";
    
    /**
     * Application context object
     */
    private FileSystemXmlApplicationContext ctx;
    
    /**
     * Data provider method  that returns list of provider aliases
     * 
     * @return Array of objects data
     */
    @DataProvider
    public Object[][] beansList() {
        
        return new Object[][] {
            {"provider_database"},
            {"provider_fixer"},
            {"provider_grandtrunk"}
        };
    }    

    /**
     * Test for check creation object of provider
     * 
     * @param providerId Provider alias
     */
    @Test(
        dataProvider = "beansList"
    )
    public void beansInit(String providerId) {

        ctx.getBean(providerId);
    }
    
    /**
     * Getting rates check 
     * 
     * @param providerId Provider alias
     * @throws WrongCurrencyPairException
     * @throws LoadExchangeRatesException
     * @throws ParseException 
     */
    @Test(
        dataProvider = "beansList",
        dependsOnMethods = "beansInit"
    )
    public void getRates(String providerId)
            throws WrongCurrencyPairException,
                   LoadExchangeRatesException,
                   ParseException {

        ExchangeRatesProvider exchangeRatesProvider = 
            (ExchangeRatesProvider) ctx.getBean(providerId);
        
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2016-10-10");
        CurrencyPair pair = new CurrencyPair("EURUSD");
        
        List<ExchangeRate> rates = exchangeRatesProvider.get(pair, date, date);
        
        assertEquals(rates.size(), 1);
    }

    @BeforeMethod
    public void setUpClass() {

        ctx = new FileSystemXmlApplicationContext(APP_CONTEXT);
        ctx.refresh();
    }
    
    @AfterMethod
    public void tearDownClass() {
        
        ctx = null;
    }
}
